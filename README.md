#### Only twin numbers in array

Array are given. Implement a function for keep only twin numbers in array. If given argument not an array - "Passed argument is not a array or empty".If given argument not contains twin numbers - "Passed array not have twin numbers in array"

Exp:
- twinArrayNumbers([1, 2, 3, 4, 5, 6]) // 2,4,6
- twinArrayNumbers([28, 44, 11, 22, 13, 76]) // 28,44,22,76
- twinArrayNumbers([1, 11, 47]) // 'Passed array not have twin numbers in array'
- twinArrayNumbers([5]) // 'Passed array not have twin numbers in array'
- twinArrayNumbers([]) //'Passed argument is not an array or empty'
- twinArrayNumbers(55) //'Passed argument is not an array or empty'

For run:

1. Clone project to a local repository
2. Run npm install
3. Implement function
4. Run npm test
<hr>
